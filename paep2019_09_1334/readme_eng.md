Bulletin of the National Technical University "KhPI".  
**Series: Problems of automated electrodrive. Theory and practice**  
No 9 (1334) 2019

The publication is devoted to covering the issues of the theory of
electromechanical systems, advances in the field of control systems of
automated electric drive and its components, energy saving using
electric drive. Published articles are devoted to the specialists
preparing in the automation of electromechanical systems and electric
drive, mechatronics and robotics and made for researchers, teachers of
higher education, graduate students, students and specialists in the
field of automated electric drive systems and its constituent elements.

[PDF][CoverPDF] [DjVU][CoverDjVU] [JPG][CoverJPG] Cover

[PDF][fullPDF]Â [DjVU][fullDjVU] Complete collection of articles in one file

[PDF][zipPDF]Â [DjVU][zipDjVU] All individual articles in one ZIP archive

[ISO][isoALL] All materials in one ISO file

---

[PDF][p001]Â [DjVU][d001] Cover sheets

---

## Science and education ##

[PDF][p00]Â [DjVU][d00]   **M. V. Miyusov**  
                         75 years of higher engineering education of seafarers in Ukraine

[PDF][p01]Â [DjVU][d01]   **A. K. Solodenko, V. P. Pogrebnyak, E. V. Dashkovskaya**  
                         National agency for the quality assurance of higher education -- the long way to create

[PDF][p02]Â [DjVU][d02]   **M. V. Anishchenko, I. M. Palchyk, V. M. Shamardina**  
                         Development remote laboratory of the "Automated electromechanical systems" department NTU "KhPI"

## Theoretical issues of automated electric drive ##
                         
[PDF][p03]Â [DjVU][d04]   **O. I. Sheremet**  
                         Analytical review of traditional and modern methods of automated electromechanical systems synthesis

[PDF][p04]Â [DjVU][d04]   **O. L. Derets, O. V. Sadovoy**  
                         Parameters correction of quasi-optimal in speed third order sliding mode control systems, synthesized by N-I switching method

[PDF][p05]Â [DjVU][d05]   **Ð. N. Sinchuk, I. Ð. Sinchuk**  
                         Theoretical and methodological substances of diagnostic prognosis of electric power consumption levels of underground iron ore enterprises

[PDF][p06]Â [DjVU][d06]   **T. Yu. Kunchenko, A. V. Pirozhok, Yu. N. Kutovoj, I. V. Obruch, A. V. Kascheev**  
                         Synthesis of a polynomial method of an astatic velocity regulator for a quasistatic method for studying an induction motor

## Modern systems of automated electric drive ##

[PDF][p07]Â [DjVU][d07]   **S. Peresada, V. Reshetnyk, D. Rodkin, O. Zinchenko**  
                         Linearizing speed control and self-commissioning of interior permanent magnet synchronous motor

[PDF][p08]Â [DjVU][d08]   **O. I. Tolochko, O. O. Burmelov, D. A. Danilov**  
                         Design of the observer for sensorless control system of the nonsalient permanent magnet motor

[PDF][p09]Â [DjVU][d09]   **O. Chornyi, V. Tutiuk, Iu. Zachepa, S. Serhiienko, E. Burdilnaya**  
                         Features of operation of the frequency-regulated electric drive when connecting the motor to a long power cable

[PDF][p10]Â [DjVU][d10]   **V. O. Lebedjev, O. M. Khalimovskyy**  
                         Research of the opportunities for performance improvement for electricdrive mechanism for supply of mechanized equipment for arc welding and surfacing

## Components of an automated electric drive ##

[PDF][p11]Â [DjVU][d11]   **V. V. Hrabko, O. V. Didushok**  
                         Investigation of the work of the electromagnetic actuator of the vacuum circuit breaker as a object diagnostis

[PDF][p12]Â [DjVU][d12]   **B. M. Gorkunov, S. G. Lvov, D. V. Hladchenko, Saliba Abdel Nour**  
                         Recognition of the structure of materials of cylindrical samples by their electromagnetic parameters

[PDF][p13]Â [DjVU][d13]   **D. Pshenychnykov, B. Vorobiov**  
                         Asynchronous electric drive power converter model of an electric vehicle in regenerative braking mode

[PDF][p14]Â [DjVU][d14]   **L. V. Asmolova, M. V. Anishchenko, K. Y. Loboda**  
                         Sensors of measurement of shaft rotation angle on the basis of equipment National Instruments on the board QNET-MECHKIT "Sensors for mechatronics"

## Energy efficiency of electromechanical systems ##

[PDF][p15]Â [DjVU][d15]   **P. D. Andrienko, O. V. Nemykina, A. A. Andrienko**  
                         Electromagnetic compatibility in operation of group cranes with Variable Frequency Drive

[PDF][p16]Â [DjVU][d16]   **A. V. Osichev, A. A. Tkachenko, B. D. Pochapsky**  
                         Methods and software for the selection of asynchronous motors, taking into account their technical characteristics in educational design

[PDF][p17]Â [DjVU][d17]   **A. V. Semikov, V. V. Voinov**  
                         To select parameters of electric vehicle electric motor

[PDF][p18]Â [DjVU][d18]   **V. B. Klepikov, Y. V. Sakun, D. A. Kurochkin**  
                         Motor control in electric vehicle with energy efficient transmission

---

[PDF][p19]Â [DjVU][d19]   Anniversaries

[PDF][p20]Â [DjVU][d20]   Content

[PDF][p21]Â [DjVU][d21]   Publisher's imprint

[CoverPDF]: paep2019_09_1334_cover.pdf
[CoverDjVU]: paep2019_09_1334_cover.djvu
[CoverJPG]: paep2019_09_1334_cover.jpg
[fullPDF]: paep2019_09_1334.pdf
[fullDjVU]: paep2019_09_1334.djvu
[zipPDF]: paep2019_09_1334_pdf.zip
[zipDjVU]: paep2019_09_1334_djvu.zip
[isoALL]: paep2019_09_1334.iso
[p001]: pdf/00_1_Ð¢Ð¸ÑÑÐ».pdf
[d001]: djvu/00_1_Ð¢Ð¸ÑÑÐ».djvu
[p00]: pdf/00_ÐÑÐ¸Ð²ÐµÑÑÑÐ²Ð¸Ðµ.pdf
[d00]: djvu/00_ÐÑÐ¸Ð²ÐµÑÑÑÐ²Ð¸Ðµ.djvu
[p01]: pdf/01_Ð¡Ð¾Ð»Ð¾Ð´ÐµÐ½ÐºÐ¾_ÐÐ¾Ð³ÑÐµÐ±Ð½ÑÐº_ÐÐ°ÑÐºÐ¾Ð²ÑÐºÐ°Ñ.pdf
[d01]: djvu/01_Ð¡Ð¾Ð»Ð¾Ð´ÐµÐ½ÐºÐ¾_ÐÐ¾Ð³ÑÐµÐ±Ð½ÑÐº_ÐÐ°ÑÐºÐ¾Ð²ÑÐºÐ°Ñ.djvu
[p02]: pdf/02_ÐÐ½Ð¸ÑÐµÐ½ÐºÐ¾_ÐÐ°Ð»ÑÑÐ¸Ðº_Ð¨Ð°Ð¼Ð°ÑÐ´Ð¸Ð½Ð°.pdf
[d02]: djvu/02_ÐÐ½Ð¸ÑÐµÐ½ÐºÐ¾_ÐÐ°Ð»ÑÑÐ¸Ðº_Ð¨Ð°Ð¼Ð°ÑÐ´Ð¸Ð½Ð°.djvu
[p03]: pdf/03_Ð¨ÐµÑÐµÐ¼ÐµÑ.pdf
[d03]: djvu/03_Ð¨ÐµÑÐµÐ¼ÐµÑ.djvu
[p04]: pdf/04_ÐÐµÑÐµÑ_Ð¡Ð°Ð´Ð¾Ð²Ð¾Ð¹.pdf
[d04]: djvu/04_ÐÐµÑÐµÑ_Ð¡Ð°Ð´Ð¾Ð²Ð¾Ð¹.djvu
[p05]: pdf/05_Ð¡Ð¸Ð½ÑÑÐº_Ð¡Ð¸Ð½ÑÑÐº.pdf
[d05]: djvu/05_Ð¡Ð¸Ð½ÑÑÐº_Ð¡Ð¸Ð½ÑÑÐº.djvu
[p06]: pdf/06_ÐÑÐ½ÑÐµÐ½ÐºÐ¾_ÐÐ¸ÑÐ¾Ð¶Ð¾Ðº_ÐÑÑÐ¾Ð²Ð¾Ð¹_ÐÐ±ÑÑÑ_ÐÐ°ÑÐµÐµÐ².pdf
[d06]: djvu/06_ÐÑÐ½ÑÐµÐ½ÐºÐ¾_ÐÐ¸ÑÐ¾Ð¶Ð¾Ðº_ÐÑÑÐ¾Ð²Ð¾Ð¹_ÐÐ±ÑÑÑ_ÐÐ°ÑÐµÐµÐ².djvu
[p07]: pdf/07_ÐÐµÑÐµÑÐ°Ð´Ð°_Ð ÐµÑÐµÑÐ½Ð¸Ðº_Ð Ð¾Ð´ÑÐºÐ¸Ð½_ÐÐ¸Ð½ÑÐµÐ½ÐºÐ¾.pdf
[d07]: djvu/07_ÐÐµÑÐµÑÐ°Ð´Ð°_Ð ÐµÑÐµÑÐ½Ð¸Ðº_Ð Ð¾Ð´ÑÐºÐ¸Ð½_ÐÐ¸Ð½ÑÐµÐ½ÐºÐ¾.djvu
[p08]: pdf/08_Ð¢Ð¾Ð»Ð¾ÑÐºÐ¾_ÐÑÑÐ¼ÐµÐ»ÐµÐ²_ÐÐ°Ð½Ð¸Ð»Ð¾Ð².pdf
[d08]: djvu/08_Ð¢Ð¾Ð»Ð¾ÑÐºÐ¾_ÐÑÑÐ¼ÐµÐ»ÐµÐ²_ÐÐ°Ð½Ð¸Ð»Ð¾Ð².djvu
[p09]: pdf/09_Ð§ÐµÑÐ½ÑÐ¹_Ð¢ÑÑÑÐº_ÐÐ°ÑÐµÐ¿Ð°_Ð¡ÐµÑÐ³Ð¸ÐµÐ½ÐºÐ¾_ÐÑÑÐ´Ð¸Ð»ÑÐ½Ð°Ñ.pdf
[d09]: djvu/09_Ð§ÐµÑÐ½ÑÐ¹_Ð¢ÑÑÑÐº_ÐÐ°ÑÐµÐ¿Ð°_Ð¡ÐµÑÐ³Ð¸ÐµÐ½ÐºÐ¾_ÐÑÑÐ´Ð¸Ð»ÑÐ½Ð°Ñ.djvu
[p10]: pdf/10_ÐÐµÐ±ÐµÐ´ÐµÐ²_Ð¥Ð°Ð»Ð¸Ð¼Ð¾Ð²ÑÐºÐ¸Ð¹.pdf
[d10]: djvu/10_ÐÐµÐ±ÐµÐ´ÐµÐ²_Ð¥Ð°Ð»Ð¸Ð¼Ð¾Ð²ÑÐºÐ¸Ð¹.djvu
[p11]: pdf/11_ÐÑÐ°Ð±ÐºÐ¾_ÐÐ¸Ð´ÑÑÐ¾Ðº.pdf
[d11]: djvu/11_ÐÑÐ°Ð±ÐºÐ¾_ÐÐ¸Ð´ÑÑÐ¾Ðº.djvu
[p12]: pdf/12_ÐÐ¾ÑÐºÑÐ½Ð¾Ð²_ÐÑÐ²Ð¾Ð²_ÐÐ»Ð°Ð´ÑÐµÐ½ÐºÐ¾_Ð¡Ð°Ð»Ð¸Ð±Ð°ÐÐ±Ð´ÐµÐ»ÑÐÑÑ.pdf
[d12]: djvu/12_ÐÐ¾ÑÐºÑÐ½Ð¾Ð²_ÐÑÐ²Ð¾Ð²_ÐÐ»Ð°Ð´ÑÐµÐ½ÐºÐ¾_Ð¡Ð°Ð»Ð¸Ð±Ð°ÐÐ±Ð´ÐµÐ»ÑÐÑÑ.djvu
[p13]: pdf/13_ÐÑÐµÐ½Ð¸ÑÐ½Ð¸ÐºÐ¾Ð²_ÐÐ¾ÑÐ¾Ð±ÑÑÐ².pdf
[d13]: djvu/13_ÐÑÐµÐ½Ð¸ÑÐ½Ð¸ÐºÐ¾Ð²_ÐÐ¾ÑÐ¾Ð±ÑÑÐ².djvu
[p14]: pdf/14_ÐÑÐ¼Ð¾Ð»Ð¾Ð²Ð°_ÐÐ½Ð¸ÑÐµÐ½ÐºÐ¾_ÐÐ¾Ð±Ð¾Ð´Ð°.pdf
[d14]: djvu/14_ÐÑÐ¼Ð¾Ð»Ð¾Ð²Ð°_ÐÐ½Ð¸ÑÐµÐ½ÐºÐ¾_ÐÐ¾Ð±Ð¾Ð´Ð°.djvu
[p15]: pdf/15_ÐÐ½Ð´ÑÐ¸ÐµÐ½ÐºÐ¾_ÐÐµÐ¼ÑÐºÐ¸Ð½Ð°_ÐÐ½Ð´ÑÐ¸ÐµÐ½ÐºÐ¾_ÐÐ²Ð´ÐµÐµÐ²_ÐÑÐ¸ÑÐ½Ð¾.pdf
[d15]: djvu/15_ÐÐ½Ð´ÑÐ¸ÐµÐ½ÐºÐ¾_ÐÐµÐ¼ÑÐºÐ¸Ð½Ð°_ÐÐ½Ð´ÑÐ¸ÐµÐ½ÐºÐ¾_ÐÐ²Ð´ÐµÐµÐ²_ÐÑÐ¸ÑÐ½Ð¾.djvu
[p16]: pdf/16_ÐÑÐ¸ÑÐµÐ²_Ð¢ÐºÐ°ÑÐµÐ½ÐºÐ¾_ÐÐ¾ÑÐ°Ð¿ÑÐºÐ¸Ð¹.pdf
[d16]: djvu/16_ÐÑÐ¸ÑÐµÐ²_Ð¢ÐºÐ°ÑÐµÐ½ÐºÐ¾_ÐÐ¾ÑÐ°Ð¿ÑÐºÐ¸Ð¹.djvu
[p17]: pdf/17_Ð¡ÐµÐ¼Ð¸ÐºÐ¾Ð²_ÐÐ¾Ð¸Ð½Ð¾Ð².pdf
[d17]: djvu/17_Ð¡ÐµÐ¼Ð¸ÐºÐ¾Ð²_ÐÐ¾Ð¸Ð½Ð¾Ð².djvu
[p18]: pdf/18_ÐÐ»ÐµÐ¿Ð¸ÐºÐ¾Ð²_Ð¡Ð°ÐºÑÐ½_ÐÑÑÐ¾ÑÐºÐ¸Ð½.pdf
[d18]: djvu/18_ÐÐ»ÐµÐ¿Ð¸ÐºÐ¾Ð²_Ð¡Ð°ÐºÑÐ½_ÐÑÑÐ¾ÑÐºÐ¸Ð½.djvu
[p19]: pdf/19_Ð®Ð±Ð¸Ð»ÐµÐ¸.pdf
[d19]: djvu/19_Ð®Ð±Ð¸Ð»ÐµÐ¸.djvu
[p20]: pdf/20_Ð¡Ð¾Ð´ÐµÑÐ¶Ð°Ð½Ð¸Ðµ.pdf
[d20]: djvu/20_Ð¡Ð¾Ð´ÐµÑÐ¶Ð°Ð½Ð¸Ðµ.djvu
[p21]: pdf/21_ÐÑÑÐ¾Ð´Ð½ÑÐµ_Ð´Ð°Ð½Ð½ÑÐµ.pdf
[d21]: djvu/21_ÐÑÑÐ¾Ð´Ð½ÑÐµ_Ð´Ð°Ð½Ð½ÑÐµ.djvu
