Bulletin of the National Technical University "KhPI".  
**Series: Problems of automated electrodrive. Theory and practice**  
No 16 (1341) 2019

The publication is devoted to covering the issues of the theory of
electromechanical systems, advances in the field of control systems of
automated electric drive and its components, energy saving using
electric drive. Published articles are devoted to the specialists
preparing in the automation of electromechanical systems and electric
drive, mechatronics and robotics and made for researchers, teachers of
higher education, graduate students, students and specialists in the
field of automated electric drive systems and its constituent elements.

[PDF][CoverPDF] [DjVU][CoverDjVU] [JPG][CoverJPG] Cover

[PDF][fullPDF]Â [DjVU][fullDjVU] Complete collection of articles in one file

[PDF][zipPDF]Â [DjVU][zipDjVU] All individual articles in one ZIP archive

[ISO][isoALL] All materials in one ISO file

---

[PDF][p001]Â [DjVU][d001] Cover sheets

[PDF][p01]Â [DjVU][d01]   ÐÑÐ¸Ð²ÐµÑÑÑÐ²Ð¸Ðµ

---

## Science and education ##

[PDF][p01]Â [DjVU][d01] **M. V. Anishchenko, S. O. Alokhin**  
Creation the pocket labs on the department of "Automated electromechanical systems" NTU "KhPI"

[PDF][p02] [DjVU][d02] **Ð. Chornyi, S. Serhiienko, O. Kravetc, A. Yudina**  
Evaluation of the efficiency of the execute of students tasks on computer training systems in the subject "Electromechanics"

[PDF][p03] [DjVU][d03] **M. Yu. Vorontsova, V. O. Kotlyarov**  
The use of mechatronic devices design tools for compiling programs of training courses

## Theoretical issues of automated electric drive ##

[PDF][p04] [DjVU][d04] **R. S. Voliansky, A. V. Sadovoy, Yu. Yu. Shramko, Yu. V. Sokhina, N. V. Volianskaya**  
Synthesis of a digital control system for a linear electromechanical object in canonical phase space

[PDF][p05]Â [DjVU][d05] **M. Ostroverkhov, M. Buryk**  
Robust control the rotor mechanical angular speed of surface mounted permanent magnet synchronous moto

[PDF][p06] [DjVU][d06] **V. Busher**  
The method of calculating the fractional integral with the dynamic error correction for microcontrollers

[PDF][p07] [DjVU][d07] **D. Rodkin, Ð¢. Korenkova, V. Kovalchuk**  
To the theory of electromechanical systems identification by the energy method

[PDF][p08]Â [DjVU][d08] **A. Y. Kazurova**  
A comparison of the dynamic characteristics of the state vector and uncertainty observers

[PDF][p09]Â [DjVU][d09] **V. V. Osadchyy, O. S. Nazarova, M. O. Oleinikov**  
Research of positional electrical drive of the two-mass system with internal following contour

[PDF][p10]Â [DjVU][d10] **N. A. Rudenko, Y. V. Zachepa**  
Identification of nonlinear parameters of induction motor in the start-up powered supplied from electromechanical energy storage device

## Components of an automated electric drive ##

[PDF][p11]Â [DjVU][d11] **Yu. P. Samcheleev, H. S. Bielokha**  
Single-phase voltage and power sources with relay control

[PDF][p12]Â [DjVU][d12] **O. S. Nazarova, V. V. Osadchyy, I. A. Meleshko, M. O. Oleinikov**  
Identification of angular velocity at interferences in the optical encoder system

## Energy efficiency of electromechanical systems ##

[PDF][p13]Â [DjVU][d13] **V. S. Petrushyn, J. R. Plotkin, R. N. Yenoktaiev, Bendahmane Boukhalfa**  
Development of energy--efficient asynchronous electric drive for intermittent operation

[PDF][p14]Â [DjVU][d14] **O. V. Naboka, P. D. Andrienko**  
Improving the energy efficiency of the power supply of auxiliary electric drives of ED9m electric trains

[PDF][p15]Â [DjVU][d15] **S. Mikhaykov, M. Mukha, A. Drankova**  
Improving the electric energy quality in operation modes of ship's cooling syste

[PDF][p16]Â [DjVU][d16] **A. Kipenskyi, I. Korol, N. Gorovykh**  
Improvement the energy performance of a three-phase thyristor converter for electric heating devices

## Modern systems of automated electric drive ##

[PDF][p17]Â [DjVU][d17] **Yu. N. Kutovoj, T. Yu. Kunchenko, I. V. Obruch, Ya. A. Kyrylenko**  
Study of the starting modes of the frequency-controlled electric drive of the main electric locomotive DS3

[PDF][p18]Â [DjVU][d18] **I. Z. Shchur**  
Active power steering system in electronic differential of electric vehicle with individual drive of two front wheels

[PDF][p19]Â [DjVU][d19] Content
[PDF][p20]Â [DjVU][d20] Publisher's imprint

[CoverPDF]: paep2019_16_1341_cover.pdf
[CoverDjVU]: paep2019_16_1341_cover.djvu
[CoverJPG]: paep2019_16_1341_cover.jpg
[fullPDF]: paep2019_16_1341.pdf
[fullDjVU]: paep2019_16_1341.djvu
[zipPDF]: paep2019_16_1341_pdf.zip
[zipDjVU]: paep2019_16_1341_djvu.zip
[isoALL]: paep2019_16_1341.iso
[p001]: pdf/00_1_Ð¢Ð¸ÑÑÐ».pdf
[d001]: djvu/00_1_Ð¢Ð¸ÑÑÐ».djvu
[p00]: pdf/00_ÐÑÐ¸Ð²ÐµÑÑÑÐ²Ð¸Ðµ.pdf
[d00]: djvu/00_ÐÑÐ¸Ð²ÐµÑÑÑÐ²Ð¸Ðµ.djvu
[p01]: pdf/01_ÐÐ½Ð¸ÑÐµÐ½ÐºÐ¾_ÐÐ»ÐµÑÐ¸Ð½.pdf
[d01]: djvu/01_ÐÐ½Ð¸ÑÐµÐ½ÐºÐ¾_ÐÐ»ÐµÑÐ¸Ð½.djvu
[p02]: pdf/02_Ð§ÐµÑÐ½ÑÐ¹_Ð¡ÐµÑÐ³Ð¸ÐµÐ½ÐºÐ¾_ÐÑÐ°Ð²ÐµÑ_Ð®Ð´Ð¸Ð½Ð°.pdf
[d02]: djvu/02_Ð§ÐµÑÐ½ÑÐ¹_Ð¡ÐµÑÐ³Ð¸ÐµÐ½ÐºÐ¾_ÐÑÐ°Ð²ÐµÑ_Ð®Ð´Ð¸Ð½Ð°.djvu
[p03]: pdf/03_ÐÐ¾ÑÐ¾Ð½ÑÐ¾Ð²Ð°_ÐÐ¾ÑÐ»ÑÑÐ¾Ð².pdf
[d03]: djvu/03_ÐÐ¾ÑÐ¾Ð½ÑÐ¾Ð²Ð°_ÐÐ¾ÑÐ»ÑÑÐ¾Ð².djvu
[p04]: pdf/04_ÐÐ¾Ð»ÑÐ½ÑÐºÐ¸Ð¹_Ð¡Ð°Ð´Ð¾Ð²Ð¾Ð¹_Ð¨ÑÐ°Ð¼ÐºÐ¾_Ð¡Ð¾ÑÐ¸Ð½Ð°_ÐÐ¾Ð»ÑÐ½ÑÐºÐ°Ñ.pdf
[d04]: djvu/04_ÐÐ¾Ð»ÑÐ½ÑÐºÐ¸Ð¹_Ð¡Ð°Ð´Ð¾Ð²Ð¾Ð¹_Ð¨ÑÐ°Ð¼ÐºÐ¾_Ð¡Ð¾ÑÐ¸Ð½Ð°_ÐÐ¾Ð»ÑÐ½ÑÐºÐ°Ñ.djvu
[p05]: pdf/05_ÐÑÑÑÐ¾Ð²ÐµÑÑÐ¾Ð²_ÐÑÑÐ¸Ðº.pdf
[d05]: djvu/05_ÐÑÑÑÐ¾Ð²ÐµÑÑÐ¾Ð²_ÐÑÑÐ¸Ðº.djvu
[p06]: pdf/06_ÐÑÑÐµÑ.pdf
[d06]: djvu/06_ÐÑÑÐµÑ.djvu
[p07]: pdf/07_Ð Ð¾Ð´ÑÐºÐ¸Ð½_ÐÐ¾ÑÐµÐ½ÑÐºÐ¾Ð²Ð°_ÐÐ¾Ð²Ð°Ð»ÑÑÑÐº.pdf
[d07]: djvu/07_Ð Ð¾Ð´ÑÐºÐ¸Ð½_ÐÐ¾ÑÐµÐ½ÑÐºÐ¾Ð²Ð°_ÐÐ¾Ð²Ð°Ð»ÑÑÑÐº.djvu
[p08]: pdf/08_ÐÐ°Ð·ÑÑÐ¾Ð²Ð°.pdf
[d08]: djvu/08_ÐÐ°Ð·ÑÑÐ¾Ð²Ð°.djvu
[p09]: pdf/09_ÐÑÐ°Ð´ÑÐ¸Ð¹_ÐÐ°Ð·Ð°ÑÐ¾Ð²Ð°_ÐÐ»ÐµÐ¹Ð½Ð¸ÐºÐ¾Ð².pdf
[d09]: djvu/09_ÐÑÐ°Ð´ÑÐ¸Ð¹_ÐÐ°Ð·Ð°ÑÐ¾Ð²Ð°_ÐÐ»ÐµÐ¹Ð½Ð¸ÐºÐ¾Ð².djvu
[p10]: pdf/10_Ð ÑÐ´ÐµÐ½ÐºÐ¾_ÐÐ°ÑÐµÐ¿Ð°.pdf
[d10]: djvu/10_Ð ÑÐ´ÐµÐ½ÐºÐ¾_ÐÐ°ÑÐµÐ¿Ð°.djvu
[p11]: pdf/11_Ð¡Ð°Ð¼ÑÐµÐ»ÐµÐµÐ²_ÐÐµÐ»Ð¾ÑÐ°.pdf
[d11]: djvu/11_Ð¡Ð°Ð¼ÑÐµÐ»ÐµÐµÐ²_ÐÐµÐ»Ð¾ÑÐ°.djvu
[p12]: pdf/12_ÐÐ°Ð·Ð°ÑÐ¾Ð²Ð°_ÐÑÐ°Ð´ÑÐ¸Ð¹_ÐÐµÐ»ÐµÑÐºÐ¾_ÐÐ»ÐµÐ¹Ð½Ð¸ÐºÐ¾Ð².pdf
[d12]: djvu/12_ÐÐ°Ð·Ð°ÑÐ¾Ð²Ð°_ÐÑÐ°Ð´ÑÐ¸Ð¹_ÐÐµÐ»ÐµÑÐºÐ¾_ÐÐ»ÐµÐ¹Ð½Ð¸ÐºÐ¾Ð².djvu
[p13]: pdf/13_ÐÐµÑÑÑÑÐ¸Ð½_ÐÐ»Ð¾ÑÐºÐ¸Ð½_ÐÐ½Ð¾ÐºÑÐ°ÐµÐ²_ÐÐµÐ½Ð´Ð°ÑÐ¼Ð°Ð½ÐÑÑÐ°Ð»ÑÐ°.pdf
[d13]: djvu/13_ÐÐµÑÑÑÑÐ¸Ð½_ÐÐ»Ð¾ÑÐºÐ¸Ð½_ÐÐ½Ð¾ÐºÑÐ°ÐµÐ²_ÐÐµÐ½Ð´Ð°ÑÐ¼Ð°Ð½ÐÑÑÐ°Ð»ÑÐ°.djvu
[p14]: pdf/14_ÐÐ°Ð±Ð¾ÐºÐ°_ÐÐ½Ð´ÑÐ¸ÐµÐ½ÐºÐ¾.pdf
[d14]: djvu/14_ÐÐ°Ð±Ð¾ÐºÐ°_ÐÐ½Ð´ÑÐ¸ÐµÐ½ÐºÐ¾.djvu
[p15]: pdf/15_ÐÐ¸ÑÐ°Ð¹ÐºÐ¾Ð²_ÐÑÑÐ°_ÐÑÐ°Ð½ÐºÐ¾Ð²Ð°.pdf
[d15]: djvu/15_ÐÐ¸ÑÐ°Ð¹ÐºÐ¾Ð²_ÐÑÑÐ°_ÐÑÐ°Ð½ÐºÐ¾Ð²Ð°.djvu
[p16]: pdf/16_ÐÐ¸Ð¿ÐµÐ½ÑÐºÐ¸Ð¹_ÐÐ¾ÑÐ¾Ð»Ñ_ÐÐ¾ÑÐ¾Ð²ÑÑ.pdf
[d16]: djvu/16_ÐÐ¸Ð¿ÐµÐ½ÑÐºÐ¸Ð¹_ÐÐ¾ÑÐ¾Ð»Ñ_ÐÐ¾ÑÐ¾Ð²ÑÑ.djvu
[p17]: pdf/17_ÐÑÑÐ¾Ð²Ð¾Ð¹_ÐÑÐ½ÑÐµÐ½ÐºÐ¾_ÐÐ±ÑÑÑ_ÐÐ¸ÑÐ¸Ð»ÐµÐ½ÐºÐ¾.pdf
[d17]: djvu/17_ÐÑÑÐ¾Ð²Ð¾Ð¹_ÐÑÐ½ÑÐµÐ½ÐºÐ¾_ÐÐ±ÑÑÑ_ÐÐ¸ÑÐ¸Ð»ÐµÐ½ÐºÐ¾.djvu
[p18]: pdf/18_Ð©ÑÑ.pdf
[d18]: djvu/18_Ð©ÑÑ.djvu
[p19]: pdf/19_Ð¡Ð¾Ð´ÐµÑÐ¶Ð°Ð½Ð¸Ðµ.pdf
[d19]: djvu/19_Ð¡Ð¾Ð´ÐµÑÐ¶Ð°Ð½Ð¸Ðµ.djvu
[p20]: pdf/20_ÐÑÑÐ¾Ð´Ð½ÑÐµ_Ð´Ð°Ð½Ð½ÑÐµ.pdf
[d20]: djvu/20_ÐÑÑÐ¾Ð´Ð½ÑÐµ_Ð´Ð°Ð½Ð½ÑÐµ.djvu
